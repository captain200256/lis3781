> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Assignment A4 Requirements:

1. Screenshot of A4 ERD
2. Optional Sql Code for Reports
3. Bitbucket Repo Link

#### Assignment Screenshots:

## Screenshot of A4 ERD

![A4 ERD Screenshot](img/A4_ERD.png)



#### Tutorial Links:

*Bitbucket A4:*
[A3 Bitbucket Link](https://bitbucket.org/captain200256/lis3781/src/master/A4/)