> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Assignment A5 Requirements:

1. Screenshot of ERD for A5
2. Atleast one report ( In this case Q2 )
3. Bitbucket Repo Link for LIS3781

#### Assignment Screenshots:

## Screenshot of A5 ERD:

![A5 ERD Screenshot](img/A5_ERD.png)


## Screenshot of question 2 from the required procedures:

![A5 Q2 Screenshot](img/Q2_SQL_and_Solution.png)



#### Tutorial Links:

*Bitbucket A5:*
[A5 Bitbucket Link](https://bitbucket.org/captain200256/lis3781/src/master/A5/)