> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Assignment A1 Requirements:

*Five Parts*

1. Distribution Version Control with Git and Bitbucket
2. AMPPS Installation
3. Questions
4. Entity Relashionship Diagram, and SQL code (optional)
5. Bitbucket Repo Links:

### Commands for GIT

1. Git ini - creates a new git repository.
2. Git status - displays the state of the working directory and the staging area.
3. Git add - tells git that you want to include updates to a certain file in the next commit.
4. Git commit - captures a snapshot of the projects current state.
5. Git push - uploads a local repositorys content to a remote repository.
6. Git pull - used to fetch and dowload content from a remote repository and update the local repository.
7. PWD - shows present working directory that git is set in.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*

![AMPPS Screenshot](img/AMPPS.PNG)



*Screenshot of A1 ERD*

![A1 ERD Screenshot](img/A1_ERD.PNG)


*Screenshot of MYSQL Question 1*

![Screenshot of SQL1 Example](img/q1_pic.PNG)

*Screenshot of MYSQL Question 2*

![Screenshot of SQL2 Example](img/q2_pic.PNG)

*Screenshot of MYSQL Question 3*

![Screenshot of SQL3 Example](img/q3_pic.PNG)

*Screenshot of MYSQL Question 4*

![Screenshot of SQL4 Example](img/q4_pic.PNG)

*Screenshot of MYSQL Question 5*

![Screenshot of SQL5 Example](img/q5_pic.PNG)

*Screenshot of MYSQL Question 6*

![Screenshot of SQL6 Example](img/q6_pic.PNG)



#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://support.atlassian.com/bitbucket-cloud/docs/create-a-git-repository/)
