> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Project P2 Requirements:

1. Screenshot of Mongo DB shell command (find command)
2. Screenshot of atleast one required report
3. Bitbucket Repo Link's

#### Assignment Screenshots:

## Screenshot of P2 Shell Command's

![P2 Shell Command](img/mongo_db_command.PNG)

![P2 Shell Command](img/MONGO_DB_IMPORT.PNG)



## Screenshot of required report

![Quetion 3 Screenshot](img/mongo_db_Q3.PNG)


#### Tutorial Links:

*Bitbucket A3:*
[P2 Bitbucket Link](https://bitbucket.org/captain200256/lis3781/src/master/P2/)