> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Project P1 Requirements:

1. ERD Screenshot
2. Foward Engineer ERD
3. SQL code pushed to CCI Server with data values and Inserts
4. Binary SSN numbers
5. Person Table screenshot


#### Project Screenshots:


*Screenshot of P1 ERD*

![P1 ERD Screenshot](img/PROJECT_1_ERD.PNG)

*Screenshot of P1 Person Table*

![P1 Person Table](img/person_table.PNG)


#### Tutorial Links:
*Bitbucket P1:*
[P1 Bitbucket Link](https://bitbucket.org/captain200256/lis3781/src/master/P1/)