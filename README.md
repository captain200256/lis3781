> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Class Number Requirements:

# Course Work Links:

### A1 Readme File

[A1 README.md](A1/README.md "My A1 README.md file")

1. Screenshot of ampps installation running.

2. Git command with descriptions.

3. ERD image for A1 assighnment.

4. Bitbucket repo links.

	a. This assighnment 
	
	b. Tutorial Links

5. SQL Statements


### A2 Readme File

[A2 README.md](A2/README.md "My A2 README.md file")


1. Screenshot of SQL code for LIS3781_A2

2. Populated Tables Screenshots

3. Screenshot of Users, Permisions and excecutables.

4. Bitbucket Repo Links.


### A3 Readme File

[A3 README.md](A3/README.md "My A3 README.md file")

1. Screenshot of SQL code for LIS3781_A3

2. Populated Tables Screenshots within Oracle


### P1 Readme File

[P1 README.md](P1/README.md "My P1 README.md file")

1. ERD Screenshot

2. Foward Engineer ERD

3. SQL code pushed to CCI Server with data values and Inserts

4. Binary SSN numbers

5. Person Table screenshot


### Assignment A4 Requirements:

[A4 README.md](A4/README.md "My A4 README.md file")

1. Screenshot of A4 ERD

2. Optional Sql Code for Reports

3. Bitbucket Repo Link


### Assignment A5 Requirements:

[A5 README.md](A5/README.md "My A5 README.md file")

1. Screenshot of ERD for A5

2. Atleast one report ( In this case Q2 )

3. Bitbucket Repo Link for LIS3781


### Project P2 Requirements:

[P2 README.md](P2/README.md "My P2 README.md file")

1. Screenshot of Mongo DB shell command (find command)

2. Screenshot of atleast one required report

3. Bitbucket Repo Link's
