> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS 3781 Advanced Database Management

## Christopher Estrugo

### Assignment A3 Requirements:

1. Screenshot of SQL code for LIS3781_A3
2. Populated Tables Screenshots within Oracle

#### Assignment Screenshots:

## Screenshot of A3 Code

![A3 CODE Screenshot](img/oracle_code_1.png)
![A3 CODE Screenshot](img/oracle_code_2.png)

## Screenshot Filled Tables in Oracle

![Tables Screenshot](img/customer_table_oracle.png)
![Tables Screenshot](img/commodity_table_oracle.png)
![Tables Screenshot](img/order_table_oracle.png)



#### Tutorial Links:

*Bitbucket A3:*
[A3 Bitbucket Link](https://bitbucket.org/captain200256/lis3781/src/master/A3/)